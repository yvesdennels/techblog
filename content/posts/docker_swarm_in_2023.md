---
title: "Docker Swarm in 2023"
date: 2023-01-24T15:44:08+01:00
tags: [docker,swarm]
draft: false
---
# Staying with Docker Swarm in 2023

TL;DR: Things seem to move favorably for Docker Swarm.

Last year [I started to use Docker Swarm]({{<ref "docker-swarm-in-2022.md">}}) on a single node swarm, wondering if it was
the right choice, with all attention going to Kubernetes. I went for Swarm because
the possibilities offered by Kubernetes were not worth the price to pay in complexity.

Nearly one year later, I'm happy with my choice. Docker Swarm brings enough flexibility
for me (as for a lot of situations for a lot of people I think), and deploying and managing it was a pleasure.
I learned and blogged about it here like how to [use Authelia]({{<ref "deploy_authelia_on_swarm.md">}}),
starting [with kapitan]({{<ref "starting-with-kapitan.md">}})
which I use to manage the deployment of an app on Swarm, [identify the container of a swarm task]({{<ref "identify_container_docker_swarm.md">}})
and [some more](/tags/swarm/).

It's surprisingly easy to deploy to a Swarm, and the number of apps to deploy is amazing. In fact, a lot of
projects publish a docker-compose file, which can easily be used (maybe with some minor changes) to deploy to Docker Swarm.

During most of 2022 the development of Swarm itself was minimal, but at the end of 2022 the number of [commits to the Swarmkit repo](https://github.com/moby/swarmkit/commits/master)
increased. And while Mirantis, owner of Docker Swarm, was very silent about it, [a (sponsored) article about Docker Swarm](https://thenewstack.io/docker-swarm-a-user-friendly-alternative-to-kubernetes/) by [Shaun O'Meara](https://thenewstack.io/author/shaun-omeara/), global field CTO of mirantis was published on The Next Web.
What a change compared to 2022 when Mirantis seemed embarassed to have Docker Swarm in its portfolio!

Sharing this on the Docker Swarm Slack channel, a Mirantis engineer confirmed "`Swarm is actively maintained and used in production`", with some info about their ongoing work.

2023 seems like an even better year to start using Docker Swarm! If you do, or if you have questions regarding my experience, don't hesitate to [let me know](mailto:{{< param "email">}})!

