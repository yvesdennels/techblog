---
title: "Using Remark42 as commenting system with Soupault"
date: 2024-08-19T14:07:19+02:00
tags: [swarm,remark42,soupault]
draft: false
---

I'm currently working on [Asfaload](https://www.asfaload.com) and with the aim of having a better interaction with users, we needed to add a commenting system.
As the website is also managed with [Soupault](https://soupault.app), we looked for a solution to be integrated in a static site generator, while still respecting the privacy of our visitors. That's how we got to [Remark42](https://www.remark42.com), which supports OAuth authentication with many provides, but also authentication by email, and even anonymous comments.

Despite its extensive features, its deployment is easy with a provided docker compose file, which can easily be converted to be used with Docker Swarm as used by this server (see below for the compose file). I deployed Remark42 to the URL `https://remark42.taktiki.com` and chose the site id `yvesdennels` for this blog.

Integrating it with Soupault is also very easy. The following configuration first inserts a `div` in which to place the commenting system, and then inserts the required javascript (as provided by Remark42) at the end of the body, but only in pages under `/posts/`.

```
[widgets.remark42-comment]
  widget = "insert_html"
  html = '<div id="remark42"></div>'
  selector = "main"
  section = "posts"
  include_subsections = true
  parse = true
[widgets.remark42-config]
  widget = "insert_html"
  html = '<script> var remark_config={ host: "https://remark42.taktiki.com", site_id: "yvesdennels", } </script> <script>!function(e,n){for(var o=0;o<e.length;o++){var r=n.createElement("script"),c=".js",d=n.head||n.body;"noModule"in r?(r.type="module",c=".mjs"):r.async=!0,r.defer=!0,r.src=remark_config.host+"/web/"+e[o]+c,d.appendChild(r)}}(remark_config.components||["embed"],document);</script> '
  selector = "body"
  section = "posts"
  include_subsections = true
  parse = true
```

As for the compose file to be used with Docker Swarm, here it is (it is deployed to a swarm set up with a Traefik reverse proxy as described [here](https://dockerswarm.rocks/traefik/)):
```
services:
  app:
    deploy:
      labels:
        shepherd.enable: "true"
        traefik.constraint-label: traefik-public
        traefik.docker.network: traefik-public
        traefik.enable: "true"
        traefik.http.routers.remark42s.entrypoints: https
        traefik.http.routers.remark42s.rule: Host(`remark42.taktiki.com`)
        traefik.http.routers.remark42s.tls: "true"
        traefik.http.routers.remark42s.tls.certresolver: le
        traefik.http.routers.remark42.entrypoints: http
        traefik.http.routers.remark42.middlewares: https-redirect
        traefik.http.routers.remark42.rule: Host(`remark42.taktiki.com`)
        traefik.http.services.remark42.loadbalancer.server.port: "8080"
    env_file:
      - private/remark42-github-cid
      - private/remark42-github-csec
      - private/remark42-secret
      - private/remark42-smtp
    environment:
      AUTH_ANON: "true"
      BACKUP_PATH: /backup
      DEBUG: "true"
      REMARK_URL: https://remark42.taktiki.com
      SITE: yvesdennels
    image: umputun/remark42:latest
    networks:
      - traefik-public
    volumes:
      - /data/remark42:/srv/var
      - /data/backups/remark42:/backup
  init:
    command:
      - mkdir
      - -p
      - /data/remark42/
      - /data/backups/remark42"
    deploy:
      restart_policy:
        condition: none
    image: debian
    networks:
      default: null
    volumes:
      - type: bind
        source: /data
        target: /data
        bind:
          create_host_path: true
networks:
  traefik-public:
    name: traefik-public
    external: true
```

As for the files under `private/`, they define the environment variables configuring remark42. The variables defined are:

* AUTH_GITHUB_CID
* AUTH_GITHUB_CSEC
* SECRET
* SMTP_HOST
* SMTP_PORT
* SMTP_TLS=true
* SMTP_USERNAME
* SMTP_PASSWORD
* AUTH_EMAIL_FROM
* NOTIFY_EMAIL_FROM
* AUTH_EMAIL_ENABLE
* AUTH_EMAIL_FROM
* NOTIFY_ADMINS=email
* NOTIFY_EMAIL_FROM
* ADMIN_SHARED_EMAIL
