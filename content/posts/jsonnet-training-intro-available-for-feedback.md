---
title: "Jsonnet Training Intro Available for Feedback"
date: 2022-09-13T15:39:25+02:00
tags: [jsonnet, training]
draft: false
---
If you read this blog, you might have understood [I'm a Jsonnet fan]({{< ref "starting-with-kapitan" >}}). To help people getting started with Jsonnet, I have prepared an online training.
Here's the first chapter, an introduction showing why you should be interested in Jsonnet. As this is my first online training production, your feedback is very welcome at [yvesdennels@gmail.com](mailto:"yvesdennels@gmail.com").

 {{< youtube W8kFrUOpO7s >}}

I hope this video raises you interest in Jsonnet. If you're a self-learner, go to [jsonnet.org](https://jsonnet.org) and follow the [tutorial](https://jsonnet.org/learning/tutorial.html). If you prefer to follow this training, contact me at [yvesdennels@gmail.com](mailto:"yvesdennels@gmail.com") to benotified or watch this space, the full training will soon be made available for a very reasonable fee.
