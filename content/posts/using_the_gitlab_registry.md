---
title: "Using the gitlab registry"
date: 2022-11-22T10:24:49+01:00
tags : [docker,gitlab,registry]
draft: false
---

Here's an easy workflow to use the container registry make available to Gitlab projects.
You first need to create a token. A Personal Access Token is usable with any project you have access to. A Deploy token is scoped to a group or a project.

## Personal Access Token

If you decide to use a Personal Access Token you first need to [create one](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` access.
As mentioned in the doc, you can do this on the gitlab.com instance by following [this link](https://gitlab.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api). If you are logged in Gitlab, it prefills the form (just change the name of the token ;-) ).
The username to combine with this token is your gitlab username.

## Deploy token
If you decide to use a Deploy Token, you can create it following [these instructions](https://docs.gitlab.com/ee/user/project/deploy_tokens/#create-a-deploy-token).
In this case you generate both a username and a token.

## Using the registry

You can now authenticate to the gitlab registry with this command
```
docker --config /path/to/my/projects/private_files login registry.gitlab.com
```
This will prompt you for your username (your gitlab username) and password (the Personal Access Token you just created).
As docker doesn't support multiple credentials for one registry, I pass the `--config` flag so that docker saves the credentials in the new file `/path/to/my/projects/private_files/config.json`.
The credentials are saved in clear text, so you should probably protect it, [eg with git-crypt]({{<ref "gitops-for-swarm-using-private-registry">}}).

When this is done, you can build your image locally and name them so they can be pushed to the registry:
```
docker build -t registry.gitlab.com/${username}/${project} .
```
You can them push them with
```
docker --config /path/to/my/projects/private_files push registry.gitlab.com/${username}/${project}
```
