---
title: "Docker ipv6 setup"
date: 2023-02-07T10:07:09+01:00
draft: false
tags: [docker,ipv6,networking]
---

If you enable ipv6 for your docker containers with these lines in your `/etc/docker/daemon.json` like illustrated in the [official documentation](https://docs.docker.com/config/daemon/ipv6/),
```

{
  "ip6tables": true,
  "fixed-cidr-v6": "2001:db8:1::/64"
}
```
your containers will get an ip from the range `2001:db8:1::/64`.

But this range is just used as an example in the documentation, and has nothing special. It is a normal routable range.
It is advised to not reuse that range if it was not assigned to you.

You can configure the docker daemon to use a subrange of you ipv6 range, but this brings some additional configuration requirements on the host to
have Neighbor Discovery Protocol traffic flowing from your router to the docker container. (For more details look [at this guide](https://medium.com/@skleeschulte/how-to-enable-ipv6-for-docker-containers-on-ubuntu-18-04-c68394a219a2) which documents how to do it manually for each container (Look for the command `sysctl net.ipv6.conf.ens3.proxy_ndp=1`) or by running a NDP proxy daemon). This is probably too cumbersome for your development laptop though.

The simplest solution in this case seems to be using [site-local addresses](https://en.wikipedia.org/wiki/Unique_local_address#Example) in the range `fd/8` and configure masquerading of containers traffic. This can be done by using this configuration in `daemon.json`:
```

{
  "ipv6": true,
  "experimental": true,
  "ip6tables": true,
  "fixed-cidr-v6": "fdef::/64"
}
```
The `ip6tables` configuration is still experimental at this date (2023-02-07) and requires the additional `"experimental": true` setting.
The `fd00/8` range seems to be somewhat controversial for some people, but its use seems warranted here as it facilitates the setup greatly.

Using this configuration, your containers will have access to the internet over ipv6.


