---
title: "Remove last newline with vim"
date: 2025-02-11T13:54:19+01:00
tags: [vim,howto]
draft: false
---

I was writing some tests for [Asfaload](https://www.asfaload.com) and I needed to validate that a JSON file
is generated correctly. I needed the file to be a one-line file, with no End Of Line
character (I probably could have done it an other way, working with a file having an EOL,
but let's not consider this for the sake of this blog post).

On a unix platform, when you save a file with vim, it will add a EOL character if there is none.
On Unix, it appears `eol` is a line terminator, and so [the convention](https://superuser.com/a/745135) is to have the line end with `eol`.
Finding the way to remove that last `eol` is surprisingly harder to find than I thought, hence this post.
The way to remove the `eol`` of the last line with vim is this:

1. `:set bin` or start vim with `vim -b`: switch to binary mode
2. `:set noeol`: from the vim help: `When writing a file and this option is off and the 'binary' option
	is on, or 'fixeol' option is off, no <EOL> will be written for the
	last line in the file. `

Easy to do in the end.
