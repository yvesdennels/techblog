---
title: "Recursive Cp Dropping Top Directory"
date: 2023-03-17T09:48:14+01:00
draft: true
tags: [linux,GNU,system]
---
I had a test run failing in a container because a recursive copy of a fixtures directory was dropping the opt directory in the destination copy.
To make it clearer, here's the verbose output of the command:

```
cp -rv fixtures/attachments run/
'fixtures/attachments' -> 'run/'
'fixtures/attachments/1' -> 'run/1'
'fixtures/attachments/1/6' -> 'run/1/6'
'fixtures/attachments/1/6/100000' -> 'run/1/6/100000'
'fixtures/attachments/1/6/100000/100000' -> 'run/1/6/100000/100000'
'fixtures/attachments/1/6/100000/100000/1219' -> 'run/1/6/100000/100000/1219
```

As you see, the `attachments` directory is not present in the destination.
The expected output is:
```
cp -rv fixtures/attachments run/
'fixtures/attachments' -> 'run/attachments'
'fixtures/attachments/1' -> 'run/attachments/1'
'fixtures/attachments/1/6' -> 'run/attachments/1/6'
'fixtures/attachments/1/6/100000' -> 'run/attachments/1/6/100000'
'fixtures/attachments/1/6/100000/100000' -> 'run/attachments/1/6/100000/100000'
'fixtures/attachments/1/6/100000/100000/1219' -> 'run/attachments/1/6/100000/100000/1219'
```

After some searching, I realised this behaviour is due to the fact that the destination directory (`run/` in my case) is missing.
Not very intuitive.

So, if you have a directory missing in the hierarchy of the destination of a recursive copy, check that the directory you copy to exists!
For completeness, this was using cp from the GNU coreutils.
