---
title: "Identify the container of a task in a docker swarm"
date: 2022-12-14T14:13:37+01:00
tags: [docker,swarm]
draft: false
---
When maintaining a Docker Swarm, you might need to take actions on a specific container, possibly identified by a task id logged by docker. But in Swarm, you run and manage services on multiple nodes, you don't have directly access to the containers. Here's how to get to the containers.

## 1) Identify the node running the container

You can list all tasks running for a service with `docker service ps $SERVICE_NAME`. For example:
```
$ docker service ps authelia_authelia
ID             NAME                      IMAGE      NODE        DESIRED STATE   CURRENT STATE             ERROR     PORTS
tlg6sfeob019   authelia_authelia.1       authelia   vmi938563   Running         Running 37 minutes ago
```

The ID column holds the task's ID, and the NODE column identifies the node on which the container of this task is running.

## 2) Identify the container

It is possible to get all details of a task with `docker inspect $TASK_ID`, which outputs JSON, and we can use `jq` to extract the container id, for example:
```
$ docker inspect tlg6sfeob019 | jq -r '.[0].Status.ContainerStatus.ContainerID'
5fc25bea4190e2628e8fbaa2879074ece0fa6ddb0be607ba8ae6ecaff1201a1a
```

At that time you have all info to access the container: you can ssh (or use docker over ssh) to the host `vmi938563` and inspect the container `5fc25bea4190`.

Thanks to Chris Becke on the Docker Swarm Slack for the tip!
