---
title: "Dotnet debugging with VSCodium on Linux"
date: 2022-10-19T15:30:15+02:00
tags : [dotnet,vscodium,netcoredbg]
draft: false
---
[VsCodium](https://vscodium.com/) is the FOSS version of Visual Studio Code, i.e. it is the same code without the private/proprietary addition added when Microsoft packages it.
Making it possible to package and publish VSCodium is great, but, without getting in the discussion of how lame this is, the dotnet debugger published by Micro$oft can only be used by Micro$oft published software, meaning not VSCodium. (Did you know the same restriction applies to usage of the VSCode Marketplace?)

Luckily, an open alternative exists: [netcoredbg](https://github.com/Samsung/netcoredbg) published by Samsung. (And if you wonder, there's an alternative marketplace available with [OpenVSX](https://open-vsx.org/))

And netcoredbg can easily be used in vscodium. I'm using it to debug my tests, so what I do is run my tests with the environment variable `VSTEST_HOST_DEBUG` set to `1`.
The output when running tests is then
```
Starting test execution, please wait...
A total of 1 test files matched the specified pattern.
Host debugging is enabled. Please attach debugger to testhost process to continue.
Process Id: 89114, Name: dotnet
Waiting for debugger attach...
Process Id: 89114, Name: dotnet
```
Doing this the tests don't run immediately, but wait for a debugger to attach. This debugger can be netcoredbg running from VSCodium. Here's how to do it.

First install the extension [muhammad-sammy.csharp](https://open-vsx.org/extension/muhammad-sammy/csharp) from OpenVSX, an extension which includes netcoredbg.

From the root directory of your project, create the file `.vscode/launch.json` with the content
```
{
"version": "0.2.0",
"configurations": [
        {
            "name": ".NET Core Attach (console)",
            "type": "coreclr",
            "processId": "${command:pickProcess}",
            "request": "attach",
            "console": "integratedTerminal",
            "stopAtEntry": false,
            "internalConsoleOptions": "openOnSessionStart",
            "pipeTransport": {
                "pipeCwd": "${workspaceFolder}",
                "pipeProgram": "bash",
                "pipeArgs": ["-c"],
                "debuggerPath": "/home/$USER/.vscode-oss/extensions/muhammad-sammy.csharp-1.25.0/.debugger/netcoredbg/netcoredbg",
                "quoteArgs": true
            }
        }
    ,]
}
```
Just replace `$USER` by your linux user name.
As I want to attach to my running tests, the `request` is set to `"attach"`. This configuration is derived from [a configuration I found in a Github issue](https://github.com/VSCodium/vscodium/issues/82#issue-409806641) at which you should take a look if you want to start a program in debug mode from VSCodium.

Open your project and set a breakpoint.
Once this is done, go in the `Run and Debug` tab (`Ctrl+Shift+d`) and at the top of the window you should have a `.NET Core Attach (console)` entry in the drop down list at the right of the green triangle. Select this entry and click the green triangle (or press `F5`). A field should appear requesting you to `Select the process to attach to`. Enter the process id, in our example this is `89114` and press enter. VSCodium will attach to the running process, and tests will continue running until the breakpoint is hit, at which point you'll be taken to the line of code on which the breakpoint was set.
