---
title: "Docker Swarm in 2022"
date: 2022-03-06T15:56:27+01:00
tags: [docker,swarm]
draft: false
---
# Going with Docker Swarm in 2022

TL;DR: Docker Swarm is probably good enough in a lot of cases.
See the [update for 2023]({{<ref "docker_swarm_in_2023.md">}})

Having worked professionally on setting up, customising and deploying
Kubernetes (but without pretending to be a specialist), I didn't want to go
down that path for my personal infrastructure, even if using Jsonnet was of great help (see my Jsonnet course [here](https://www.udemy.com/course/jsonnet-from-scratch/?referralCode=EFA565D6914866D5FADE)).
I tried [Nomad](https://www.nomadproject.io/) which worked fine, but getting it
to do what I wanted was taking more time than I hoped.

Then I relunctantly tried Docker Swarm. Reluctantly because I thought Docker
Swarm was dead and unusable at this point. I thought that the community of
users must be so small and shrinking that getting help would be hard. But that
last point, if probably true, appeared irrelevant. Things are so easy to set up
that every step worked at first try and I had multiple apps available in a
couple of hours.

Sure, my requirements are simple: make it as easy as possible to deploy web
apps on one server. No multi-node, no HA. Which is probably a good chunk of web
app deployments out there. And in that case, Docker Swarm really shines. Read
[Single Node Swarms Are
Awesome](https://jarredkenny.com/blog/single-node-docker-swarm) for more info.

I've deployed the setup described at [Docker Swarm
Rocks](https://dockerswarm.rocks/), with [Traefik as reverse
proxy](https://dockerswarm.rocks/traefik/). No need to duplicate that content
here, so go there if you want to try it out.

The biggest advantage is that to deploy app to your Docker Swarm, you can use a
docker-compose file. The great thing is that a majority of projects propose a
`docker-compose.yml` file for deployment, making it readily available to you
for deployment on your swarm. You might have to edit it slightly, but it is
    much more approachable than translating it to another format like K8s
    manifests.

Of course, Docker Swarm is not developed very actively, but the speed at which
I could deploy my infrastructure makes it worth the bet. And if I have to
migrate to another solution later on, I won't have lost a lot of time anyway.

Take a look and decide for yourself!
