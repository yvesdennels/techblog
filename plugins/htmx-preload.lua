-- Preload links with htmx: https://htmx.org/extensions/preload/#configuration
-- Example config on soupault.toml:
-- [widgets.htmx-preload]
--  widget = "htmx-preload"
--  selector = "a[href^='/']" # default values are given
--  htmx_script_tag = '<script src="https://unpkg.com/htmx.org@1.9.8" integrity="sha384-EAzY246d6BpbWR7sQ8+WEm40J8c3dHFsqC58IgPlh4kMbRRI6P6WA+LA/qGAyAu8" crossorigin="anonymous"></script>'
--  preload_script_tag = '<script src="https://unpkg.com/htmx.org/dist/ext/preload.js" integrity="sha384-NoiTdF/KjxH4wa6OHMdM4F6e/LIPNMcFBgOIINTTaczWo0vX1mWTOzckySqlGQS/" crossorigin="anonymous"></script>'
-- Selector identifies the elements that need to get the preload attribute
selector = config["selector"]
if not selector then
  selector = "a[href^='/']"
  Log.warning("htmx preload selector is not configured, using default " .. selector)
else
  Log.info("htmx preload selector is " .. selector)
end

-- Set the event on which the preload is triggered
preload_event = config["preload_event"]
if not preload_event then
  preload_event = "mousedown"
  Log.warning("htmx preload event is not configured, using default " .. preload_event)
else
  Log.info("htmx preload event is " .. preload_event)
end

-- Identify elements to which we need to add the preload attribute
preloads = HTML.select(page, selector)
preloads_count = size(preloads)

-- Function to determine is htmx is already loaded
function has_htmx()
  local htmx = HTML.select(page, "script[src*=\"htmx.org\"]")
  return  size(htmx)>0
end

-- Function to determine is the proload plugin is already loaded
function has_preload()
  local preload = HTML.select(page, "script[src*=\"preload.js\"]")
  return size(preload)>0
end

-- Function to load hmx and the preload plugin if needed
function load_htmx_preload ()
  if not (has_htmx()) then
    head = HTML.select_one(page, "head")
    if head then
      -- build script tag
      htmx_script_tag = config["htmx_script_tag"]
      if not htmx_script_tag then
        htmx_script= HTML.parse([[<script src="https://unpkg.com/htmx.org@1.9.8" integrity="sha384-EAzY246d6BpbWR7sQ8+WEm40J8c3dHFsqC58IgPlh4kMbRRI6P6WA+LA/qGAyAu8" crossorigin="anonymous"></script>]])
      else
        htmx_script= HTML.parse(htmx_script_tag)
      Log.info("Inserting htmx script node in head")
      HTML.append_child(head, htmx_script)
    else
      Log.error("no head found in document")
    end
  end
  if not (has_preload()) then
    head = HTML.select_one(page, "head")
    if head then
      preload_script_tag = config["preload_script_tag"]
      if not preload_script_tag then
        preload_script = HTML.parse([[<script src="https://unpkg.com/htmx.org/dist/ext/preload.js" integrity="sha384-NoiTdF/KjxH4wa6OHMdM4F6e/LIPNMcFBgOIINTTaczWo0vX1mWTOzckySqlGQS/" crossorigin="anonymous"></script>]])
      else
        htmx_script= HTML.parse(preload_script_tag)
      end
      Log.info("Inserting htmx preload script node in head")
      HTML.append_child(head, preload_script)
    else
      Log.error("no head found in document")
    end
  end

  body  = HTML.select_one(page, "body")
  HTML.set_attribute(body,"hx-ext","preload")
end

-- Function to set the preload attribute to elements matching the selector
function set_preload ()
  local i = 1
  while i <= preloads_count do
    HTML.set_attribute(preloads[i],"preload",preload_event)
    i = i+1
  end
end

-- Do the work
if preloads_count > 0 then
  load_htmx_preload()
  set_preload()
end
