---
title: "Edit a file on a docker swarm volume"
date: 2022-08-28T15:35:18+02:00
tags: [docker,swarm]
draft: false
---
Editing a file stored on a container volume in production is a very bad idea. You should not do it and pass all configuration through your container orchestrator.
Using a single node Docker Swarm for test purposes, it can be a huge time saver though.

Here's how to do it. Ensure that your `DOCKER_HOST` environment is set such
that running `docker volume ls` will show the volume storing the file you want
to edit (let's call this volume `myvol`).
Accessing the file is actually very easy by running
```
docker run --rm -it -v myvol:/data ubuntu bash
```
You'll find the content of the volume under the directory `/data`. You might first need to install your preferred editor if the image you run does not include it.
