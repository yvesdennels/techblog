---
title: "Websharper compilation problem after dotnet update"
date: 2023-02-15T11:08:54+01:00
tags : [fsharp,websharper]
draft: false
---

If you encounter problems compiling your [WebSharper](https://www.websharper.com) project after an update of your dotnet installation, you might have to restart the service `wsfscservice`.
Under linux you can simply issue the command `pkill wsfscservice`.

`wsfscservice` is used to speed up repeated compilation. As explained by [Jand42](https://github.com/Jand42):`it uses AssemblyLoadContext for WS macros/offline site generator, but F# compiler part can sadly lock some dlls for TPs and it seems SDK update messes with it too`.

In my case, after an upgrade from dotnet 6.0.405 to 6.0.406, I couldn't compile the project and got pages of errors of this kind:

``` WebSharper error WS9001 : Global error :
System.Reflection.ReflectionTypeLoadException: Unable to load one or more of
the requested types.Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.  at
System.Reflection.RuntimeModule.GetTypes(RuntimeModule module)   at
System.Reflection.RuntimeAssembly.get_DefinedTypes()   at
WebSharper.Compiler.FSharp.CodeReader.SymbolReader.ResolveAnonRecord(FSharpAnonRecordTypeDetails
d)   at WebSharper.Compiler.FSharp.CodeReader.SymbolReader.ReadTypeSt(Boolean
markStaticTP, FSharpMap`2 tparams, FSharpType t)   at
WebSharper.Compiler.FSharp.CodeReader.SymbolReader.ReadType(FSharpMap`2
tparams, FSharpType t)   at
WebSharper.Compiler.FSharp.CodeReader.SymbolReader.ReadMember(FSharpMemberOrFunctionOrValue
x, FSharpOption`1 cls)   at
WebSharper.Compiler.FSharp.ProjectReader.getAnnot@278(SymbolReader sr,
FSharpEntity cls, TypeAnnotation annot, Dictionary`2 annotations,
FSharpMemberOrFunctionOrValue x)   at
WebSharper.Compiler.FSharp.ProjectReader.transformClass(Lazy`1 sc, Compilation
comp, ResolveFuncArgs ac, SymbolReader sr, Dictionary`2 classAnnots,
FSharpFunc`2 isInterface, Dictionary`2 recMembers, FSharpEntity cls, List`1
members)   at
WebSharper.Compiler.FSharp.ProjectReader.transformAssembly@1308-2.Invoke(FSharpImplementationFileContents
file)   at Microsoft.FSharp.Collections.SeqModule.Iterate[T](FSharpFunc`2
action, IEnumerable`1 source) in D:\a\_work\1\s\src\FSharp.Core\seq.fs:line 603
at WebSharper.Compiler.FSharp.ProjectReader.transformAssembly(LoggerBase
logger, Compilation comp, String assemblyName, WsConfig config,
FSharpCheckProjectResults checkResults)   at
WebSharper.Compiler.FSharp.WebSharperFSharpCompiler.Compile(Task`1 prevMeta,
String[] argv, WsConfig config, String assemblyName, FSharpOption`1 logger)
at WebSharper.Compiler.FSharp.Compile.comp@191.Invoke(Unit unitVar0)   at
WebSharper.Compiler.AssemblyResolver.Wrap[T](FSharpFunc`2 action)   at
WebSharper.Compiler.FSharp.Compile.Compile$cont@136-1(WsConfig config,
WarnSettings warnSettings, LoggerBase logger, FSharpFunc`2 tryGetMetadata,
String thisName, FSharpChecker checker, Boolean isBundleOnly, String[]
jsCompilerArgs, Unit unitVar)   at
WebSharper.Compiler.FSharp.Compile.Compile$cont@100(WsConfig config,
WarnSettings warnSettings, LoggerBase logger, FSharpFunc`2 checkerFactory,
FSharpFunc`2 tryGetMetadata, String thisName, Unit unitVar)   at
WebSharper.Compiler.FSharp.Compile.Compile(WsConfig config, WarnSettings
warnSettings, LoggerBase logger, FSharpFunc`2 checkerFactory, FSharpFunc`2
tryGetMetadata)   at
Program.compilationResultForDebugOrRelease@152(FSharpFunc`2 checkerFactory,
FSharpFunc`2 tryGetMetadata, LoggerBase logger, WsConfig wsConfig, WarnSettings
warnSettings, String[] args, Unit unitVar0)System.BadImageFormatException:
Could not load file or assembly 'Microsoft.Extensions.Identity.Core,
Version=6.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'. Reference
assemblies should not be loaded for execution.  They can only be loaded in the
Reflection-only loader context. (0x80131058)File name:
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60' ---> System.BadImageFormatException: Cannot
load a reference assembly for execution.System.BadImageFormatException: Could
not load file or assembly 'Microsoft.Extensions.Identity.Stores,
Version=6.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'. Reference
assemblies should not be loaded for execution.  They can only be loaded in the
Reflection-only loader context. (0x80131058)File name:
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60' ---> System.BadImageFormatException: Cannot
load a reference assembly for execution.System.BadImageFormatException: Could
not load file or assembly 'Microsoft.Extensions.Identity.Core, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60'. Reference assemblies should
not be loaded for execution.  They can only be loaded in the Reflection-only
loader context. (0x80131058)File name: 'Microsoft.Extensions.Identity.Core,
Version=6.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Stores, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Core, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Core, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly
'Microsoft.Extensions.Identity.Core, Version=6.0.0.0, Culture=neutral,
PublicKeyToken=adb9793829ddae60'. Reference assemblies should not be loaded for
execution.  They can only be loaded in the Reflection-only loader context.
(0x80131058)File name: 'Microsoft.Extensions.Identity.Core, Version=6.0.0.0,
Culture=neutral, PublicKeyToken=adb9793829ddae60' --->
System.BadImageFormatException: Cannot load a reference assembly for
execution.System.BadImageFormatException: Could not load file or assembly ```
