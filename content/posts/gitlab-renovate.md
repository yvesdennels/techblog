---
title: "Using Renovate with Gitlab"
date: 2024-02-28T14:44:19+01:00
tags: [gitlab,renovate]
draft: false
---

Here's a step by step guide to using [Renovate](https://docs.renovatebot.com/) with [Gitlab](https://www.gitlab.com). These notes are based on the Renovate Runner's [documentation](https://gitlab.com/renovate-bot/renovate-runner/#renovate-runner). The documentation is complete, but still required me to wonder how exactly to set it up. This is probably my fault, but if you're like me, this guide will help you set it up without thinking much.

## Create a gitlab account for your Renovate runner

Just [register an account at Gitlab](https://gitlab.com/users/sign_up), eg `my-renovate-bot-account`.

## Create Personal Access Token

With the just created user, [create a Personal Access Token](https://gitlab.com/-/user_settings/personal_access_tokens) with access rights `read_user`, `api`, `write_repository` ([documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)). Copy the token value to use it later.

It is also advised to create a Github Personal access token as it will allow renovate to include change logs from the originating project in the merge requests it will create. You can create this token without adding any scope as it will only access public information. Copy its value to us it later.

## Create a repo

With the just created user account, create a repository which will be used to run the Renovate CI job.
First create a Group and then a repo, eg `Renovate-Repos / renovate-runner`. Clone it locally

## Push a .gitlab-ci.yml

In the just created repo; create file `.gitlab-ci.yml` with content
```
include:
    - project: 'renovate-bot/renovate-runner'
      file: '/templates/renovate.gitlab-ci.yml'
```

or see [here](https://gitlab.com/renovate-bot/renovate-runner/#create-a-gitlab-ci-file) for more advanced configs.

## Create a scheduled pipeline

Under `build` > `pipeline schedules`, create a new pipeline.
Choose the schedule you want, and create 2, possibly 3, variables:

* `RENOVATE_TOKEN`, with value the personal access token you created above
* `RENOVATE_EXTRA_FLAGS`, which configure how and which repos are discovered. To discover all repos `my-renovate-bot-account` has access to, set the value to `--autodiscover=true`. For more fine-grained configuration, see [here](https://gitlab.com/renovate-bot/renovate-runner/#configure-cicd-variables).
* `GITHUB_COM_TOKEN`, which will result in merge requests including change logs for updates of dependencies stored in Github if you generated a Github personal access token above. This is optional.

## Make you renovate user a member of projects

Add `my-renovate-bot-account` as member of the projects you want to renovate (under `Manage` > `Members`).

## Run your scheduled pipeline manually

This will run the pipeline and do the onboarding if needed. The onboarding is creating a merge request with a default config file for renovate. This merge request also gives an overview of the current state of the repo, and the updates that will be proposed in merge requests.
